#!/bin/bash

#                 * * * OBNAM BACKUP REMINDER * * *
# Version 0.1                                 by Heiko Mueller, Mar 2016
#
# This script is intended to run automatically after booting. It then
# checks if the last successful backup as registered in the obnam log-
# file ($LOGFILE) is older than a specified number of hours ($ALERTTIME).
# If so, a notfication is raised through the notify-send application
# reminding the user to renew his backups.

# VARIABLES
LOGFILE="$HOME/obnam.log"  # Location of obnam log file.
PATTERN=' INFO Backup finished.$'  # Log pattern indicating successful backup.
ALERTTIME=168  # Number of hours since last backup before reminder pops up.

# CHECK IF PROPER OBNAM LOGFILE EXISTS...
if [ ! -e $LOGFILE ]; then  # File doesn't exist.
	exit 1

elif [ ! -s $LOGFILE ]; then  # File is empty ("not not" zero ("! -s")).
	exit 1

# ...IF SO, EVALUATE LAST BACKUP DATE.
else
	# Extract date of last successful backup from LOGFILE.
	BAKDATE=$(grep "$PATTERN" $LOGFILE | tail --lines=1 | cut --fields=1,2 --delimiter=" ")
	#echo "BAKDATE= $BAKDATE"  # for debugging.
	
	# Convert backup date into seconds since epoch (01.01.1970 00:00).
	BAKEPOCH=$(date --date="$BAKDATE" +%s)
	#echo "BAKEPOCH= $BAKEPOCH"  # for debugging.
	
	# Find difference between backup date and current time.
	NOWEPOCH=$(date +%s)  # Current time in seconds since epoch.
	#echo "NOWEPOCH= $NOWEPOCH"  # for debugging.
	DIFF=$((($NOWEPOCH - $BAKEPOCH) / 3600))  # Seconds into hours.
	#echo "DIFF= $DIFF"  # for debugging.
	
	if [ $DIFF -gt $ALERTTIME ]; then
		sleep 2m  # Delay notification 2 min until OS is fully booted.
		notify-send --expire-time=12000 "Datensicherung erneuern!" "Die letzte Sicherung erfolgte vor $DIFF Stunden, am $(date --date="$BAKDATE" +%x)."
		exit 1
	fi
	
	exit 1
fi
